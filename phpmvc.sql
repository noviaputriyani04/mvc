-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 30 Okt 2023 pada 23.34
-- Versi server: 8.0.30
-- Versi PHP: 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `phpmvc`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `blog`
--

CREATE TABLE `blog` (
  `id` int NOT NULL,
  `penulis` varchar(255) NOT NULL,
  `judul` varchar(255) NOT NULL,
  `tulisan` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data untuk tabel `blog`
--

INSERT INTO `blog` (`id`, `penulis`, `judul`, `tulisan`) VALUES
(3, 'Linux', 'Belajar PHP MVC', 'Tutorial PHP MVC'),
(4, 'Linux', 'Belajar OOP PHP', 'Tutorial OOP PHP'),
(5, 'Linux', 'Belajar PHP Dasar', 'Tutorial PHP Dasar');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int NOT NULL,
  `username` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`) VALUES
(3, 'admin', 'sdmllsdm', '$2y$10$4omPgQ9dTzkRPPgPBX4l/OtwR7A8ZYcz/z0UzDkgHtTM3vlTvzJMK'),
(4, 'novia', 'noviaputriyani@gmail.com', '$2y$10$sc5xJ3SnSPIh35XN6/hEYOwcefGDnRhvA7SUU/61wiqq3gyeIReSK'),
(5, 'nesa', 'nesapuspita@gmail.com', '$2y$10$YlagQv2dJupl7pJajWXd5.FTse/j4NTGkUEwgap1tnpwDQ4oZPKYC'),
(6, 'manis', 'admin@fgi.co.id', '$2y$10$lf5A/Ntq2.emfelxIMyFXOE6WlKEn52Ja.yFxqj2Afo.76P3uUPee'),
(7, 'agus', 'agus@gmail.com', '$2y$10$4T4y6Af6A3sf06740OtXo.8UMjpOj/DBfH.4zAxUrieSloEvKQIhq');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
