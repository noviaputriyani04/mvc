<?php


class Login extends Controller{
    private $table = 'users'; 
    private $db;
    public function __construct()
    {
        $this->db = new Database;
    }

    
    public function index() {
        // if (isset($_SESSION['login'])) {
        //     header('Location: ' . BASE_URL . '/home');
        // }
        $data["judul"] = "Login";
        // $data["nama"] = $this->model("User_model")->getUser();
        // $this->view("templates/header", $data);
        $this->view("login/index", $data);
        // $this->view("templates/footer");
    }

    public function login(){

        $username = $_POST["username"];
        $passwordPost = $_POST["password"];
        $queryUser = "SELECT * FROM users WHERE username = :username";
        $this->db->query($queryUser);
        $this->db->bind('username', $username);
        $user = $this->db->single();
        $passwordDb = $user['password'];
        if (password_verify($passwordPost, $passwordDb)) {
            header('Location: ' . BASE_URL . '/home');
        }else{
            header('Location: ' . BASE_URL . '/Login/login');
        }
    }

    public function register(){

    }

    

}