<?php

class User extends Controller{

    public function __construct()
    {
        if (!isset($_SESSION['login'])) {
            header('location: ' . BASE_URL . '/login');
        }
    }   
public function index() {
    $data['judul'] = "User";
    $this->view("templates/header", $data);
    $this->view("user/index");
    $this->view("templates/footer");
}

public function profile ($nama = "Novia", $pekerjaan = "Siswa") {
    $data["judul"] = "Profile";
    $data["nama"] = $nama;
    $data["pekerjaan"] = $pekerjaan;
    $this->view("templates/header", $data);
    $this->view("user/profile", $data);
    $this->view("templates/footer");
}

}