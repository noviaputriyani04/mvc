<!-- <div class="container"> -->
<div class="mt-3  p-5 banner" style="background-color: #ECE3CE;">
    <div class="container">
        <h1 class=" text-center">Selamat Datang</h1>
        <!-- <p class="lead">Perkenalkan nama saya <?= $data["nama"]; ?></p> -->
        <hr class="my-4">
        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Non molestias magni nobis! Esse, odio tenetur! Natus
            tempora quibusdam maiores beatae blanditiis aperiam eveniet corrupti qui laudantium, minus maxime laborum
            repellat.</p>
        <a class="btn btn-light btn-lg"  href="<?=BASE_URL;?>" role="button">Lear More</a>
    </div>
</div>
<!-- </div> -->

<h3 class="text-center mt-4 ">FAVORITE BOOKS</h3>
<div class="container mt-4">
<div class="row">
    <div class="col-lg-3">
        <div class="card" style="width: 18rem;">
        <img src="<?= BASE_URL; ?>/img/buku1.jpeg " alt="" width="250px" height="250px" class="card-img-top">
            <div class="card-body">
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                    card's content.</p>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card" style="width: 18rem;">
        <img src="<?= BASE_URL; ?>/img/buku2.jpeg " alt="" width="250px" height="250px" class="card-img-top">
            <div class="card-body">
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                    card's content.</p>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card" style="width: 18rem;">
        <img src="<?= BASE_URL; ?>/img/buku3.jpeg " alt="" width="250px" height="250px" class="card-img-top">
            <div class="card-body">
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                    card's content.</p>
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <div class="card" style="width: 18rem;">
        <img src="<?= BASE_URL; ?>/img/buku4.jpeg " alt="" width="250px" height="250px" class="card-img-top">
            <div class="card-body">
                <p class="card-text">Some quick example text to build on the card title and make up the bulk of the
                    card's content.</p>
            </div>
        </div>
    </div>
</div>
</div>



<!-- <style>
    .banner {
        min-height: 100vh;
        background-image: url('<?= BASE_URL; ?>/img/pohon.jpeg');
        background-size: cover;
        background-position: center;
    }
</style> -->