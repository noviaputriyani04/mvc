<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?=$data["judul"];?></title>
    <link rel="stylesheet" href="http://localhost/belajarmvc/public/css/bootstrap.css">
    </link>
</head>

<body>
    <div class="container">
    <nav class="navbar navbar-expand-lg bg-body-tertiary">
        <div class="container-fluid">
            <a class="navbar-brand" href="<?=BASE_URL;?>">MVC</a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav"
                aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse row" id="navbarNav">
                <ul class="navbar-nav col-lg-11">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="<?=BASE_URL;?>">Home</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=BASE_URL;?>/about">About</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=BASE_URL;?>/blog">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?=BASE_URL;?>/user/profile">User</a>
                    </li>
                </ul>
                <div class="col-lg-1 mt-2">
                <?php if (isset($_SESSION['login'])) : ?>
                        <button type="logout" class="btn btn-primary" id="logout-button"><a href="<?= BASE_URL; ?>/Login/logout" class="text-white text-decoration-none">Login</a></button>
                    <?php else : ?>
                        <button type="login" class="btn btn-primary" id="login-button"><a href="<?= BASE_URL; ?>/login" class="text-white text-decoration-none">logout</a></button>
                    <?php endif; ?>
                    </div>
            </div>
        </div>
    </nav>
    </div>