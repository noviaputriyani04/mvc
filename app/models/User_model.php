<?php 
class User_model 
{ 
    private $table = 'users'; 
    private $db; 
 
    public function getUser() 
    { 
        return $this->table; 
    } 
    public function __construct() 
    { 
        $this->db = new Database; 
    } 
    // public function register($data) 
    // { 
    //     $query = "INSERT INTO users (username, password) VALUES (:username, :password)"; 
    //     $passwordHash = password_hash($data['password'], PASSWORD_DEFAULT); 
    //     $this->db->query($query); 
    //     $this->db->bind('username', $data['username']); 
    //     $this->db->bind('password', $passwordHash); 
    //     $this->db->execute(); 
 
    //     return 1; 
    // } 
 
    public function login($data) 
    { 
        $query = "SELECT * FROM users WHERE Username = :username"; 
        $this->db->query($query); 
        $this->db->bind('username', $data['username']); 
        $user = $this->db->single(); 
        $passwordPost = $data['password']; 
        $passwordDb = $user['Password']; 
        if (password_verify($passwordPost, $passwordDb)) { 
            return 1; 
        } else { 
            return -1; 
        } 
    } 
}
